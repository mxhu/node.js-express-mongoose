const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    email: { type: String, required: true },
    surname: { type: String, required: false },
    firstName: { type: String, required: false }
});

module.exports = mongoose.model('User', userSchema);