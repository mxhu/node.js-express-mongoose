var mongoose = require('mongoose');
var postSchema = mongoose.Schema({
  title: {
    type: String
  },
  description: {
    type: String
  },
  approved: {
    type: Boolean,
    default: false
  },
  created_at: {
    type: Date,
    default: Date.now()
  }
})

module.exports = mongoose.model('Post', postSchema)