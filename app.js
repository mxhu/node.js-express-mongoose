const hbs = require('hbs');
const bodyParser = require('body-parser');

// import config
const config = require('./config'); 

// import models
const Post = require('./models/post');
const UserModel = require('./models/user');

//mongoose.connect('mongodb://localhost/whateverdb');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.connect(config.dbConnection);
const db = mongoose.connection;

db.on('err', function() {
  console.log("there was an error connection to db")
})

db.once('open', function() {
  console.log("connected to db")
})

// express server
const express = require('express');
const app = express();

app.listen(3000, function() {
  console.log('server started on port 3000')
})

// set view engine
app.set('view engine', 'hbs')

//middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

// routes
app.get('/', function(req, res) {
  Post.find({}, function(err, postss) {
    res.render('index', {
      posts: postss
    })
  })
})

// try promise
app.post('/user', function(req,res) {
  let newUser = new UserModel();
  newUser.email = req.body.email;
  newUser.save().then(
    resp => {
      console.log(resp);
      return Post.find({});
    }
  ).then(
    resp=> {
      console.log(resp);
    }
  ).catch(
    error => { 
      console.log(error);
    }
  )
  res.send('user added')
})

app.post('/posts/add', function(req, res) {
  let newPost = new Post()
  newPost.title = req.body.title
  newPost.description = req.body.description
  newPost.save(function() {
    console.log('successfully saved post to db')
    res.redirect('/')
  })
})
